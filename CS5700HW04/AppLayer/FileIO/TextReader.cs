﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AppLayer.Grid;

namespace AppLayer.FileIO
{
    public class TextReader
    {
        private StreamReader sr;

        public TextReader(string filename)
        {
            try
            {
                sr = new StreamReader(filename);
            }
            catch(Exception e)
            {
                sr = new StreamReader("InputTextFiles/Puzzle-9x9-0001.txt");
            }
            
        }

        private static bool IsAlphaNumeric(string strToCheck)
        {
            Regex rg = new Regex(@"^[a-zA-Z0-9\s,]*$");
            return rg.IsMatch(strToCheck);
        }

        public void ReadGameBoard(SudokuGrid grid)
        {
            grid.Size = int.Parse(sr.ReadLine());

            grid.InitGrid();
            grid.Values = new string[grid.Size];

            string temp = sr.ReadLine();
            string[] gridValues = temp.Split(' ');

            for (int i = 0; i < gridValues.Length; i++)
            {
                grid.Values[i] = gridValues[i];
            }

            for (int i = 0; i < grid.Size; i++)
            {
                temp = sr.ReadLine();
                gridValues = temp.Split(' ');
                for (int j = 0; j < gridValues.Length; j++)
                {
                    if (IsAlphaNumeric(gridValues[j]))
                    {
                        int n;
                        if (int.TryParse(gridValues[j], out n))
                        {
                            grid.SetGridLocation(n, i + 1, j + 1);
                        }
                        else
                        {
                            char c = gridValues[j][0];
                            int index = (c % 32) + 9;
                            grid.SetGridLocation(index, i + 1, j + 1);
                        }
                    }
                    else
                        grid.SetGridLocation(0, i + 1, j + 1);
                }
            }
            sr.Close();
        }
    }
}
