﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppLayer.Grid
{
    internal class RowColumnValue
    {
        private int row;

        internal int Row
        {
            get { return row; }
        }

        private int column;

        internal int Column
        {
            get { return column; }
        }

        private int value;

        internal int Value
        {
            get { return value; }
        }

        internal RowColumnValue(int r, int c, int v)
        {
            row = r;
            column = c;
            value = v;
        }
    }
}
