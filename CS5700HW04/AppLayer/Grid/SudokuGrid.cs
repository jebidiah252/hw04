﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppLayer.Grid
{
    public class SudokuGrid
    {
        public int Size { get; set; }
        public string[] Values { get; set; }
        public int[,] Grid { get; set; }

        public void InitGrid()
        {
            Grid = new int[Size, Size];
        }

        public int GetGridLocation(int row, int column)
        {
            return Grid[row - 1, column - 1];
        }

        public void SetGridLocation(int value, int row, int column)
        {
            Grid[row - 1, column - 1] = value;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine();

            for (int i = 1; i <= Size; i++)
            {
                for (int j = 1; j <= Size; j++)
                {
                    if (GetGridLocation(i, j) > 9)
                    {
                        sb.Append((char)(GetGridLocation(i, j) + 56) + " ");
                    }
                    else if(GetGridLocation(i, j) == 0)
                    {
                        sb.Append("- ");
                    }
                    else
                    {
                        sb.Append(GetGridLocation(i, j) + " ");
                    }
                }
                sb.AppendLine();
            }
            return sb.ToString();
        }
    }
}
