﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppLayer.Grid;

namespace AppLayer.SolvingAlgorithms
{
    public class BackTrackingAlgorithm
    {
        private double timeElapsed;

        public void OutputTime()
        {
            Console.WriteLine("It took " + timeElapsed / 1000 + " to try and solve the puzzle");
        }
        public void Solve(SudokuGrid grid)
        {
            timeElapsed = 0;
            var watch = System.Diagnostics.Stopwatch.StartNew();
            for (int column = 1; column <= grid.Size; column++)
            {
                for (int row = 1; row <= grid.Size; row++)
                {
                    if (TrySolving(grid, row, column))
                    {
                        row = 1;
                        column = 1;
                    }
                }
            }
            watch.Stop();
            timeElapsed = watch.ElapsedMilliseconds;
        }

        public bool CheckIfSolved(SudokuGrid grid)
        {
            for (int column = 1; column <= grid.Size; column++)
            {
                for (int row = 1; row < grid.Size; row++)
                {
                    if(grid.GetGridLocation(row, column) == 0)
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        private bool TrySolving(SudokuGrid grid, int row, int column)
        {
            List<RowColumnValue> possibleValuesFound = new List<RowColumnValue>();

            if (grid.GetGridLocation(row, column) == 0)
            {
                for (int possiblevalues = 1; possiblevalues <= grid.Size; possiblevalues++)
                {
                    if (!DoesRowContainValue(grid, possiblevalues, row, column))
                    {
                        if (!DoesColumnContainValue(grid, possiblevalues, row, column))
                        {
                            if (!DoesSquareContainValue(grid, possiblevalues, row, column))
                            {
                                possibleValuesFound.Add(new RowColumnValue(row, column, possiblevalues));
                            }
                        }
                    }
                }

                if (possibleValuesFound.Count == 1)
                {
                    grid.SetGridLocation(possibleValuesFound[0].Value, possibleValuesFound[0].Row, possibleValuesFound[0].Column);

                    return true;
                }
            }
            return false;
        }

        private bool DoesSquareContainValue(SudokuGrid grid, int value, int row, int column)
        {
            int rowStart = ((row - 1) / (int)Math.Sqrt(grid.Size)) + 1;
            int columnStart = ((column - 1) / (int)Math.Sqrt(grid.Size)) + 1;
            int rowIndexEnd = rowStart * (int)Math.Sqrt(grid.Size);

            if (rowIndexEnd == 0)
                rowIndexEnd = (int)Math.Sqrt(grid.Size);

            int rowIndexStart = rowIndexEnd - ((int)Math.Sqrt(grid.Size) - 1);
            int columnIndexEnd = columnStart * (int)Math.Sqrt(grid.Size);

            if (columnIndexEnd == 0)
                columnIndexEnd = (int)Math.Sqrt(grid.Size);

            int columnIndexStart = columnIndexEnd - ((int)Math.Sqrt(grid.Size) - 1);

            for (int rowIndex = rowIndexStart; rowIndex <= rowIndexEnd; rowIndex++)
            {
                for (int columnIndex = columnIndexStart; columnIndex <= columnIndexEnd; columnIndex++)
                {
                    if ((grid.GetGridLocation(rowIndex, columnIndex) == value) & (columnIndex != column) & (rowIndex != row))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private bool DoesColumnContainValue(SudokuGrid grid, int value, int row, int column)
        {
            for (int rowindex = 1; rowindex <= grid.Size; rowindex++)
            {
                if ((grid.GetGridLocation(rowindex, column) == value) & row != rowindex)
                {
                    return true;
                }
            }

            return false;
        }

        private bool DoesRowContainValue(SudokuGrid grid, int value, int row, int column)
        {
            for (int columnindex = 1; columnindex <= grid.Size; columnindex++)
            {
                if ((grid.GetGridLocation(row, columnindex) == value) & column != columnindex)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
