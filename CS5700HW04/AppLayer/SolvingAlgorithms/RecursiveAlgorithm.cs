﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppLayer.Grid;

namespace AppLayer.SolvingAlgorithms
{
    public class RecursiveAlgorithm
    {
        private static int Size;

        public RecursiveAlgorithm(int size)
        {
            Size = size;
        }

        public bool PleaseSolve(SudokuGrid grid)
        {

            return Solve(grid, 0, 0);
        }

        private bool Solve(SudokuGrid grid, int row, int col)
        {
            Console.WriteLine("Looking at grid point: " + row + ", " + col);
            if (row < Size && col < Size)
            {
                if (grid.GetGridLocation(row + 1, col + 1) != 0)
                {
                    if ((col + 1) < Size)
                        return Solve(grid, row, col + 1);
                    else if ((row + 1) < Size)
                        return Solve(grid, row + 1, 0);
                    else
                        return true;
                }
                else
                {
                    for (int i = 0; i < Size; i++)
                    {
                        if (IsAvailable(grid, row, col, i + 1))
                        {
                            grid.SetGridLocation(i + 1, row + 1, col + 1);

                            if ((col + 1) < Size)
                            {
                                if (Solve(grid, row, col + 1))
                                    return true;
                                else
                                    grid.SetGridLocation(0, row + 1, col + 1);
                            }
                            else if ((row + 1) < Size)
                            {
                                if (Solve(grid, row + 1, 0))
                                    return true;
                                else
                                    grid.SetGridLocation(0, row + 1, col + 1);
                            }
                            else
                                return true;
                        }
                    }
                }
                return false;
            }
            else
                return true;
        }

        private static bool IsAvailable(SudokuGrid grid, int row, int col, int num)
        {
            int rowStart = (row / (int)Math.Sqrt(Size)) * (int)Math.Sqrt(Size);
            int colStart = (col / (int)Math.Sqrt(Size)) * (int)Math.Sqrt(Size);

            for(int i = 0; i < Size; i ++)
            {
                if (grid.GetGridLocation(row + 1, i + 1) == num)
                    return false;
                if (grid.GetGridLocation(i + 1, col + 1) == num)
                    return false;
                if (grid.GetGridLocation(rowStart + (i % (int)Math.Sqrt(Size)) + 1, colStart + (i / (int)Math.Sqrt(Size)) + 1) == num)
                    return false;
            }

            return true;
        }
    }
}
