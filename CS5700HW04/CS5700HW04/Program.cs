﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppLayer.FileIO;
using AppLayer.Grid;
using AppLayer.SolvingAlgorithms;

namespace CS5700HW04
{
    class Program
    {
        static void Main(string[] args)
        {
            TextReader readerOne = new TextReader("InputTextFiles/Puzzle-4x4-0001.txt");
            SudokuGrid myGrid = new SudokuGrid();
            BackTrackingAlgorithm algorithmOne = new BackTrackingAlgorithm();
            readerOne.ReadGameBoard(myGrid);
            algorithmOne.Solve(myGrid);
            algorithmOne.OutputTime();
            if (algorithmOne.CheckIfSolved(myGrid))
            {
                Console.WriteLine("The puzzle has been solved!");
                DisplayBoard(myGrid);
            }
            else
            {
                Console.WriteLine("The puzzle couldn't be solved");
            }
            TextReader readerTwo = new TextReader("InputTextFiles/Puzzle-9x9-0001.txt");
            readerTwo.ReadGameBoard(myGrid);
            BruteForceAlgorithm algorithmTwo = new BruteForceAlgorithm(myGrid.Size);
            if(algorithmTwo.Solve(myGrid))
            {
                DisplayBoard(myGrid);
                Console.WriteLine();
            }
            TextReader readerThree = new TextReader("InputTextFiles/Puzzle-16x16-0001.txt");
            readerThree.ReadGameBoard(myGrid);
            RecursiveAlgorithm algorithmThree = new RecursiveAlgorithm(myGrid.Size);
            if(algorithmThree.PleaseSolve(myGrid))
            {
                DisplayBoard(myGrid);
            }
            Console.Read();
        }

        public static void DisplayBoard(SudokuGrid grid)
        {
            for (int i = 0; i < grid.Size; i++)
            {
                for (int j = 0; j < grid.Size; j++)
                {
                    Console.Write(grid.Grid[i, j] + " ");
                }
                Console.WriteLine();
            }
        }
    }
}
