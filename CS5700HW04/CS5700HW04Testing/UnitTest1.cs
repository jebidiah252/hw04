﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using AppLayer.Grid;
using AppLayer.SolvingAlgorithms;
using AppLayer.FileIO;
using CS5700HW04;

namespace CS5700HW04Testing
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            TextReader myReader = new TextReader("InputTextFiles/Puzzle-9x9-0001.txt");
            SudokuGrid myGrid = new SudokuGrid();
            myReader.ReadGameBoard(myGrid);
            RecursiveAlgorithm myAlgorithm = new RecursiveAlgorithm(myGrid.Size);
            Assert.IsTrue(myAlgorithm.PleaseSolve(myGrid));
        }

        [TestMethod]
        public void TestMethod2()
        {
            TextReader myReader = new TextReader("InputTextFiles/Puzzle-9x9-0001.txt");
            SudokuGrid myGrid = new SudokuGrid();
            myReader.ReadGameBoard(myGrid);
            BruteForceAlgorithm myAlgorithm = new BruteForceAlgorithm(myGrid.Size);
            Assert.IsTrue(myAlgorithm.Solve(myGrid));
        }

        [TestMethod]
        public void TestMethod3()
        {
            TextReader myReader = new TextReader("InputTextFiles/Puzzle-9x9-0001.txt");
            SudokuGrid myGrid = new SudokuGrid();
            myReader.ReadGameBoard(myGrid);
            BackTrackingAlgorithm myAlgorithm = new BackTrackingAlgorithm();
            myAlgorithm.Solve(myGrid);
            Assert.IsTrue(myAlgorithm.CheckIfSolved(myGrid));
        }
    }
}
